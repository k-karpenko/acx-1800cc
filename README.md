Required tools
=========================

- `SBT` (http://scala-sbt.org) used as a project building infrastructure provider

Overview
=========================

- Tests

- Run tests suite

`> sbt test`

- Build distributable package

`$> sbt universal:packageBin`

- Launch program

`$> ./bin/cc1800 -charset conf/dictionary.txt -words conf/words.txt -file /opt/data.txt`

- Interactive input mode

`$> ./bin/cc1800 -charset conf/dictionary.txt -words conf/words.txt `

- Debug output mode

`$> ./bin/cc1800 -charset conf/dictionary.txt -words conf/words.txt -reportPrinter debug`

- Help

`$>./bin/cc1800 --help`

Design specifics
================

- Completely immutable application state; with only a single `var` used

- Low cohesion level: independent logic entities specialized just on a some particular activity (matching, reading,
loading, processing) hidden behind interface

-

- `Streams` used instead of strict collections in order to deal with recursion-related `StackOverflowException` problem

Implementation specifics
========================

- When application started in `interactive` mode, `@finish` should be the last line coming after the input data

- Output data lines are prefixed with '1-800-'

- Input data lines will be cleaned from all prefixes incl. 1-800, 1-800-, 0-800, 0-800-, 1800, 0800

- Data outputs to the StdOut