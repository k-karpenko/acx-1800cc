name := "aconex-cc1800"

scalaVersion := "2.11.7"
scalacOptions ++= Seq("-deprecation")

enablePlugins(JavaAppPackaging)

topLevelDirectory := Some("cc1800")

mappings in Universal ++= Seq(
  file("src/main/resources/100k-words.txt") → "./data/100k-words.txt",
  file("src/main/resources/charset.txt") → "./data/charset.txt",
  file("src/main/resources/input.txt") → "./data/input.txt"
)

lazy val ClasspathPattern = "declare -r app_classpath=\"(.*)\"\n".r

bashScriptDefines :=  bashScriptDefines.value.map {
  case ClasspathPattern(classpath) => "declare -r app_classpath=\"${app_home}/../data/*:" + classpath + "\"\n"
  case _@entry => entry
}

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test"