package au.com.aconex.assignment.cc1800

import java.io.ByteArrayInputStream

import au.com.aconex.assignment.cc1800.loaders.{ClasspathStreamLoader, InputStreamLoader}
import au.com.aconex.assignment.cc1800.processor.DataProcessor.{CharsetTableSource, WordsDictionarySource}
import au.com.aconex.assignment.cc1800.processor.{DataProcessor, DefaultDataProcessor}
import au.com.aconex.assignment.cc1800.reader.{DefaultInputDataReader, InputDataReader}
import au.com.aconex.assignment.cc1800.replacement.{DefaultReplacementStrategy, ReplacementStrategy}
import au.com.aconex.assignment.cc1800.matchers._
import org.scalamock.scalatest.MockFactory
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

class DataProcessorTest extends WordSpec with Matchers with MockFactory {

  "DefaultDataProcessor" should {
    "no results when empty words list" in new Fixture {
      val inputData =
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          "225563".getBytes()
        ))

      val wordsData = DataProcessor.WordsDictionarySource(
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          "".getBytes()
        ))
      )

      withDefaultProcessor(wordsData) { processor ⇒
        withExecutionContext { ec ⇒
          val processResult = Await.result(processor.process(inputData)(ec), 200.millis )

          processResult should have size 0
        }
      }
    }

    "no results when empty input data" in new Fixture {
      val inputData =
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          "".getBytes()
        ))

      val wordsData = DataProcessor.WordsDictionarySource(
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          """
            |CALL-ME
            |CALL-MF
            |CALL-MD
          """.stripMargin.getBytes()
        ))
      )

      withDefaultProcessor(wordsData) { processor ⇒
        withExecutionContext { ec ⇒
          val processResult = Await.result(
            processor.process(inputData)(ec),
            200.millis
          )

          processResult should have size 0
        }
      }
    }

    "clear input data from all prefixes specified by input transformation" in new Fixture {
      val inputData =
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          "1800-225563\n1-800-225563\n225563".getBytes()
        ))

      val wordsData = DataProcessor.WordsDictionarySource(
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          """
            |CALL-ME
            |CALL-MF
            |CALL-MD
          """.stripMargin.getBytes()
        ))
      )

      withDefaultProcessor(wordsData) { processor ⇒
        withExecutionContext { ec ⇒
          val processResult = Await.result(processor.process(inputData)(ec), 200.millis)

          processResult should have size 3
          processResult.map(_.transformed) should contain theSameElementsAs Seq("CALL-ME", "CALL-MF", "CALL-MD")
        }
      }
    }

    "single match for a single word" in new Fixture {
      val inputData = DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
        "225563".getBytes()
      ))

      val wordData = DataProcessor.WordsDictionarySource(
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          """
            |CALL-ME
            |CALL-MZ
            |CALL-MV
          """.stripMargin.getBytes()
        ))
      )

      withDefaultProcessor(wordData) { processor ⇒
        withExecutionContext { ec ⇒
          val processResult = Await.result(processor.process(inputData)(ec), 200.millis)

          processResult should have size 1
          processResult.map(_.transformed) should contain theSameElementsAs Seq("CALL-ME")
        }
      }
    }

    "multiple matches for a single word" in new Fixture {
      val inputData = DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
        "225563".getBytes()
      ))

      val wordsData = DataProcessor.WordsDictionarySource(
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          """
            |CALL-ME
            |CALL-MF
            |CALL-MD
          """.stripMargin.getBytes()
        ))
      )

      withDefaultProcessor(wordsData) { processor ⇒
        withExecutionContext { ec ⇒
          val processResult = Await.result( processor.process(inputData)(ec), 200.millis)

          processResult should have size 3
        }
      }
    }

    "multiple matches for a multiple words" in new Fixture {
      val inputData = DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
        "n837822".getBytes()
      ))

      val wordsData = DataProcessor.WordsDictionarySource(
        DataProcessor.DataSource.InputStreamSource(new ByteArrayInputStream(
          """
            |TEST-CC
            |TEST-AC
            |TEST-AA
          """.stripMargin.getBytes()
        ))
      )

      withDefaultProcessor(wordsData) { processor ⇒
        withExecutionContext { ec ⇒
          val processResult = Await.result(processor.process(inputData)(ec), 200.millis)

          processResult should have size 3
        }
      }
    }
  }

  trait Fixture {
    final val charsetTableSource = DataProcessor.CharsetTableSource(
      DataProcessor.DataSource.PathSource(InputStreamLoader.Path.ClasspathBased("/charset.txt"))
    )

    final val testMaxDistance = 2

    case class InputData(charsetTablePath: CharsetTableSource,
                         wordsDictionaryPath: WordsDictionarySource,
                         inputStreamLoader: InputStreamLoader)

    def withDefaultProcessor(wordsData: DataProcessor.WordsDictionarySource)
                            (block: DataProcessor ⇒ Unit): Unit = {
      withInputDataReader(Some(
        InputDataReader.regexInputTransformation(InputDataReader.ignorePrefixesRegexp)
      )) { inputDataReader ⇒
        withReplacementStrategy { replacementStrategy ⇒
          withInputData(charsetTableSource, wordsData) { inputData ⇒
            withMatchingStrategyProvider { matchingStrategyProvider ⇒
              withDataProcessor(
                inputData.charsetTablePath,
                inputData.wordsDictionaryPath,
                inputData.inputStreamLoader,
                inputDataReader,
                replacementStrategy,
                matchingStrategyProvider
              ) { processor ⇒
                block(processor)
              }
            }
          }
        }
      }
    }

    def withExecutionContext(block: ExecutionContext ⇒ Unit): Unit = {
      block(ExecutionContext.global)
    }

    def withInputDataReader(transformation: Option[InputDataReader.InputTransformation])
                           (block: InputDataReader ⇒ Unit): Unit = {
      block(new DefaultInputDataReader(transformation))
    }


    def withInputData(charsetTableSource: CharsetTableSource,
                      wordsDictionarySource: WordsDictionarySource)(block: InputData ⇒ Unit): Unit = {
      block(
        InputData(charsetTableSource, wordsDictionarySource, new ClasspathStreamLoader)
      )
    }

    def withMatchingStrategyProvider(block: MatchingStrategyProvider ⇒ Unit): Unit = {
      block((charsetTable: CharsetTable, wordsTable: WordsTable) ⇒
        new DefaultStrategy(wordsTable, charsetTable))
    }

    def withReplacementStrategy(block: ReplacementStrategy ⇒ Unit): Unit = {
      block(new DefaultReplacementStrategy)
    }

    def withClasspathInputStream(block: InputStreamLoader ⇒ Unit): Unit = {
      block(new ClasspathStreamLoader)
    }

    def withDataProcessor(charsetTablePath: CharsetTableSource,
                          wordsTablePath: WordsDictionarySource,
                          inputStreamLoader: InputStreamLoader,
                          inputDataReader: InputDataReader,
                          replacementStrategy: ReplacementStrategy,
                          matchingStrategyProvider: MatchingStrategyProvider)
                         (block: DataProcessor ⇒ Unit): Unit = {
      block(new DefaultDataProcessor(
        charsetTablePath,
        wordsTablePath,
        inputStreamLoader,
        inputDataReader,
        replacementStrategy,
        matchingStrategyProvider))
    }
  }

}
