package au.com.aconex.assignment.cc1800

import java.io.InputStream

import au.com.aconex.assignment.cc1800.loaders.{ClasspathStreamLoader, CompositeStreamLoader, FilesystemBasedStreamLoader, InputStreamLoader}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.ExecutionContext

class LoadersTest extends WordSpec with Matchers with MockFactory {

  "CompositeStreamLoader" should {
    "lift child error result" in new Fixture {
      withFailingMockLoader(Some(testExceptionMessage1)) { loader ⇒
        withCompositeLoader(loader) { compositeLoader ⇒
          compositeLoader.openStream(testFilePath1).left.get.getMessage shouldBe testExceptionMessage1
        }
      }
    }

    "fail" when {
      "when child loader fails" in new Fixture {
        withFailingMockLoader() { loader ⇒
          withCompositeLoader(loader) { compositeLoader ⇒
            compositeLoader.openStream(testFilePath1).isLeft shouldBe true
          }
        }
      }

      "when empty children list provided during construction" in new Fixture {
        an[IllegalArgumentException] should be thrownBy new CompositeStreamLoader(Seq())
      }
    }

    "succeed" when {
      "when there are loaders of same type registered" in new Fixture {
        withResolvableMockLoader(testFilePath1) { firstLoader ⇒
          withResolvableMockLoader(testFilePath1) { secondLoader ⇒
            withCompositeLoader(firstLoader, secondLoader) { compositeLoader ⇒
              compositeLoader.openStream(testFilePath1).isRight shouldBe true
              compositeLoader.openStream(testFilePath1).right.get should not be (null)
            }
          }
        }
      }

      "supported path provided" in new Fixture {
        withAcceptingMockLoader { mockLoader1 ⇒
          withRefusingMockLoader { mockLoader2 ⇒
            withCompositeLoader(mockLoader1, mockLoader2) { compositeLoader ⇒
              compositeLoader.isAcceptable(testClasspath1) shouldBe true
            }
          }
        }
      }

      "supported and resolvable path provided" in new Fixture {
        withResolvableMockLoader(testFilePath1) { loader ⇒
          withCompositeLoader(loader) { compositeLoader ⇒
            compositeLoader.openStream(testFilePath1).isRight shouldBe true
          }
        }
      }
    }
  }

  "ClasspathStreamLoader" should {
    "succeed" when {
      "correct path provided" in new Fixture {
        withClasspathBasedLoader { loader ⇒
          loader.isAcceptable(testClasspath1) shouldBe true
          loader.openStream(testClasspath1).isLeft should be (false)
          loader.openStream(testClasspath1).right.get shouldNot be (null)
        }
      }

      "incorrect path provided" in new Fixture {
        withClasspathBasedLoader { loader ⇒
          loader.isAcceptable(testClasspath1) shouldBe true
          val stream = loader.openStream(testClasspath1)
          stream.isRight should be (true)
          stream.right.get shouldNot be (null)
        }
      }
    }
  }

  trait Fixture {
    final val testClasspath1 = InputStreamLoader.Path.ClasspathBased("/charset.txt")
    final val testFilePath1 = InputStreamLoader.Path.FilesystemBased("src/test/resources/charset.txt")
    final val testExceptionMessage1 = "test exception 123"

    def withGlobalEC(block: ExecutionContext ⇒ Unit): Unit = {
      block(ExecutionContext.global)
    }

    def withResolvableMockLoader(path: InputStreamLoader.Path)(block: InputStreamLoader ⇒ Unit): Unit = {
      val obj = stub[InputStreamLoader]
      (obj.openStream _).when(path).returns(Right(mock[InputStream]))
      (obj.isAcceptable _).when(path).returns(true)
      block(obj)
    }

    def withFailingMockLoader(exceptionText: Option[String] = None)(block: InputStreamLoader ⇒ Unit): Unit = {
      val obj = stub[InputStreamLoader]
      (obj.isAcceptable _).when(*).returns(true)
      (obj.openStream _).when(*).returns(Left(new IllegalStateException(exceptionText.getOrElse("test"))))
      block(obj)
    }

    def withRefusingMockLoader(block: InputStreamLoader ⇒ Unit): Unit = {
      val obj = stub[InputStreamLoader]
      (obj.isAcceptable _).when(*).returns(false)
      block(obj)
    }

    def withAcceptingMockLoader(block: InputStreamLoader ⇒ Unit): Unit = {
      val obj = stub[InputStreamLoader]
      (obj.isAcceptable _).when(*).returns(true)
      block(obj)
    }

    def withFileBasedLoader(block: InputStreamLoader ⇒ Unit): Unit = {
      block(new FilesystemBasedStreamLoader())
    }

    def withClasspathBasedLoader(block: InputStreamLoader ⇒ Unit): Unit = {
      block(new ClasspathStreamLoader())
    }

    def withCompositeLoader(children: InputStreamLoader*)(block: InputStreamLoader ⇒ Unit): Unit = {
      block(new CompositeStreamLoader(children))
    }
  }

}
