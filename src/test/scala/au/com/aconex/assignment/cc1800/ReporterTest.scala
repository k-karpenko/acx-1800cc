package au.com.aconex.assignment.cc1800

import java.io.{ByteArrayOutputStream, PrintStream, PrintWriter, StringWriter}

import au.com.aconex.assignment.cc1800.processor.DataProcessor
import au.com.aconex.assignment.cc1800.report.{PrintStreamReportPrinter, ReportPrinter}
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

class ReporterTest extends WordSpec with Matchers {

  "PrintStreamReportPrinter" should {
    "transform all output characters into the capitalized form" in new Fixture {
      withOutputStream { os ⇒
        withExecutionContext { ec ⇒
          withDefaultPrinter(new PrintStream(os), ec) { printer ⇒
            Await.ready(printer.produceReport(testResult2 #:: testResult1 #:: Stream.empty), 200.millis)
            val report = new String(os.toByteArray)
            val lines = report.split("\n")
            lines should have size 2
            lines(0) should be (testResult2.transformed.toUpperCase)
            lines(1) should be (testResult1.transformed.toUpperCase)
          }
        }
      }
    }

    "produce no output in a case of an empty results set" in new Fixture {
      withOutputStream { os ⇒
        withExecutionContext { ec ⇒
          withDefaultPrinter(new PrintStream(os), ec) { printer ⇒
            Await.ready(printer.produceReport(Stream.empty), 200.millis)
            val report = new String(os.toByteArray)
            report shouldBe empty
          }
        }
      }
    }
  }

  trait Fixture {
    val testResult1 = DataProcessor.ProcessResult("test", "test")
    val testResult2 = DataProcessor.ProcessResult("test", "test2")

    def withExecutionContext(block: ExecutionContext ⇒ Unit): Unit = {
      block(ExecutionContext.global)
    }

    def withDefaultPrinter(out: PrintStream, ec: ExecutionContext)
                          (block: ReportPrinter ⇒ Unit): Unit = {
      block(new PrintStreamReportPrinter(out)(ec))
    }

    def withOutputStream(block: ByteArrayOutputStream ⇒ Unit): Unit = {
      block(new ByteArrayOutputStream())
    }
  }

}
