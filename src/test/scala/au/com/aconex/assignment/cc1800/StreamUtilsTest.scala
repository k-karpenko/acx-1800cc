package au.com.aconex.assignment.cc1800

import java.io.{ByteArrayInputStream, InputStream, StringReader}

import org.scalatest.{Matchers, WordSpec}

class StreamUtilsTest extends WordSpec with Matchers {
  import utils._

  "loadInputStream" should {
    "correctly load data" when {
      "single line input provided" in new Fixture {
        withInputStream(singleLineInput) { is ⇒
          val lines = loadInputStream(is)
          lines should have size 1
          lines.head shouldBe singleLineInput
        }
      }

      "multi-line input provided" in new Fixture {
        val multiLines = multiLineInput.split("\\n")

        withInputStream(multiLineInput) { is ⇒
          val lines = loadInputStream(is)

          lines should have size (multiLines.length)
          lines should contain theSameElementsAs multiLines
        }
      }

      "empty input source provided" in new Fixture {
        withInputStream(emptyInput) { is ⇒
          val lines = loadInputStream(is)
          lines should have size 0
        }
      }
    }
  }

  trait Fixture {
    final val emptyInput = ""
    final val singleLineInput = "test test"
    final val multiLineInput = "test test\ntest\ntest\n"

    def withInputStream(data: String)(block: InputStream ⇒ Unit): Unit = {
      block(new ByteArrayInputStream(data.getBytes))
    }

  }

}
