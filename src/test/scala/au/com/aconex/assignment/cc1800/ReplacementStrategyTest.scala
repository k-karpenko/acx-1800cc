package au.com.aconex.assignment.cc1800

import au.com.aconex.assignment.cc1800.replacement.{DefaultReplacementStrategy, ReplacementStrategy}
import org.scalatest.{Matchers, WordSpec}

class ReplacementStrategyTest extends WordSpec with Matchers {

  "single replacement" in new Fixture {
    withStrategy { strategy ⇒
      val replacement = strategy.applyTransformations(testString2, testString1ReplacementsSet3)
      replacement shouldBe "CALL-ME"
    }
  }

  "no replacements available" in new Fixture {
    withStrategy { strategy ⇒
      val replacement = strategy.applyTransformations(testString1, Seq())

      replacement shouldBe testString1
    }
  }

  "multiple: replacements are longer then original text" in new Fixture {
    withStrategy { strategy ⇒
      val replacement = strategy.applyTransformations(testString1, testString1ReplacementsSet1)
      replacement shouldBe "CALL-ME.CALL-ME.CALL-ME.CALL-ME"
    }
  }

  "multiple: replacements are shorter then original text" in new Fixture {
    withStrategy { strategy ⇒
      val replacement = strategy.applyTransformations(testString1, testString1ReplacementsSet2)

      replacement shouldBe "CC.CC.CC.CC"
    }
  }

  trait Fixture {
    final val testString1 = "225563.225563.225563.225563"
    final val testString2 = "225563"

    final val testString1ReplacementsSet1 = Seq(
      ReplacementStrategy.Replacement(0, "CALL-ME".toCharArray, 6),
      ReplacementStrategy.Replacement(6, "CALL-ME".toCharArray, 6),
      ReplacementStrategy.Replacement(12, "CALL-ME".toCharArray, 6),
      ReplacementStrategy.Replacement(18, "CALL-ME".toCharArray, 6)
    )

    final val testString1ReplacementsSet2 = Seq(
      ReplacementStrategy.Replacement(0, "CC".toCharArray, 6),
      ReplacementStrategy.Replacement(6, "CC".toCharArray, 6),
      ReplacementStrategy.Replacement(12, "CC".toCharArray, 6),
      ReplacementStrategy.Replacement(18, "CC".toCharArray, 6)
    )

    final val testString1ReplacementsSet3 = Seq(
      ReplacementStrategy.Replacement(0, "CALL-ME".toCharArray, 6)
    )

    def withStrategy(block: ReplacementStrategy ⇒ Unit): Unit = {
      block(new DefaultReplacementStrategy)
    }
  }
}
