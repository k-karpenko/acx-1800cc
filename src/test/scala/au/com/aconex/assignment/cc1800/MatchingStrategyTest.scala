package au.com.aconex.assignment.cc1800

import Math._
import java.io.FileInputStream

import au.com.aconex.assignment.cc1800.matchers.{CharsetTable, DefaultStrategy, MatchingStrategy, WordsTable}
import org.scalatest.matchers.MatchResult
import org.scalatest.{Matchers, WordSpec}

class MatchingStrategyTest extends WordSpec with Matchers {

  "no matches" in new Fixture {
    withCharset { charset ⇒
      withWords(charset, "CALL-ME") { words ⇒
        withStrategy(words, charset) { strategy ⇒
          val matches = strategy.findMatches("225564225564")
          matches should have size 0
        }
      }
    }
  }

  "original formatting hasn't been affected by a matcher" in new Fixture {
    withCharset { charset ⇒
      withWords(charset, "CALL-ME") { words ⇒
        withStrategy(words, charset) { strategy ⇒
          val matches = strategy.findMatches("22556 3")
          matches should have size 0
        }
      }
    }
  }

  "single-word match" in new Fixture {
    withCharset { charset ⇒
      withWords(charset, "CALL-ME") { words ⇒
        withStrategy(words, charset) { strategy ⇒
          val matches = strategy.findMatches("225563-19-225563-22333333-225563")
          matches should have size 1

          withMatchResults(matches.head) { (matches, noMatches) ⇒
            matches should have size 3
            noMatches should have size 14
          }
        }
      }
    }
  }

  "single-word single-match" in new Fixture {
    withCharset { charset ⇒
      withWords(charset, "CALL-ME") { words ⇒
        withStrategy(words, charset) { strategy ⇒
          val matches = strategy.findMatches("225563-19")
          matches should have size 1

          withMatchResults(matches.head) { (matches, noMatches) ⇒
            matches should have size 1
            noMatches should have size 3
          }
        }
      }
    }
  }

  "multiple matches" in new Fixture {
    withCharset { charset ⇒
      withWords(charset, "CALL-ME", "CBLL-ME", "ABLL-M") { words ⇒
        withStrategy(words, charset) { strategy ⇒
          val matches = strategy.findMatches("225563.225563.1233225563")

          matches should have size pow(3, 3).toLong
        }
      }
    }
  }

}

trait Fixture {
  import utils._

  def withMatchResults(matches: MatchingStrategy#Match)
                      (block: (Iterable[MatchingStrategy.Result], Iterable[MatchingStrategy.Result]) ⇒ Unit): Unit =
    block(
      matches filter {
        case _: MatchingStrategy.Result.Match ⇒ true
        case _ ⇒ false
      },
      matches filter {
        case MatchingStrategy.Result.NoMatches ⇒ true
        case _ ⇒ false
      }
    )

  def withStrategy(words: WordsTable, charset: CharsetTable)(block: MatchingStrategy => Unit): Unit =
    block(new DefaultStrategy(words, charset))

  def withCharset(block: CharsetTable ⇒ Unit) =
    block(CharsetTable(loadInputStream(getClass.getResourceAsStream("/charset.txt"))))

  def withWords(charsetTable: CharsetTable, words: String*)(block: WordsTable => Unit): Unit =
    block(WordsTable(words, charsetTable))

}
