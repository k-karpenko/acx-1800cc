package au.com.aconex.assignment.cc1800

import java.io.ByteArrayInputStream

import au.com.aconex.assignment.cc1800.reader.{DefaultInputDataReader, InputDataReader}
import au.com.aconex.assignment.cc1800.matchers.CharsetTable
import org.scalatest.{Matchers, WordSpec}

class InputDataReaderTest extends WordSpec with Matchers {

  "InputDataReader" should {
    "ignore empty input lines" in new Fixture {
      withCharset { charsetTable ⇒
        withReader { reader ⇒
          val resultStream = reader.readInput(new ByteArrayInputStream(testInputWithSpaces.getBytes()),
            charsetTable)

          resultStream should have size 3
          resultStream.exists(_.isEmpty) should be (false)
        }
      }
    }

    "trim input lines" in new Fixture {
      withCharset { charsetTable ⇒
        withReader { reader ⇒
          val resultStream = reader.readInput(new ByteArrayInputStream(testInputWithSpaces.getBytes()),
            charsetTable)

          resultStream.exists(_.contains(" ")) should be (false)
        }
      }
    }

    "read large files (100k)" in new Fixture {
      withCharset { charsetTable ⇒
        withReader { reader ⇒
          val array = 0 to 100 * 1000
          val input = array map { i ⇒ i.toString } mkString "\n"
          val resultStream = reader.readInput(new ByteArrayInputStream(input.getBytes),
            charsetTable)

          resultStream should have size array.filterNot(v ⇒ v.toString.contains("1") || v.toString.contains("0")).length
        }
      }
    }

    "apply passed transformation (function)" in new Fixture {
      withCharset { charsetTable ⇒
        withTransformationReader(InputDataReader.inputTransformation("1-800", "1-800-")) { reader ⇒
          val resultStream = reader.readInput(new ByteArrayInputStream(testPrefixedInput.getBytes()),
            charsetTable)

          resultStream should have size 2
          resultStream should contain theSameElementsAs Seq("225563", "225564")
        }
      }
    }

    "apply passed transformation (regexp)" in new Fixture {
      withCharset { charsetTable ⇒
        withTransformationReader(InputDataReader.regexInputTransformation(InputDataReader.ignorePrefixesRegexp)) { reader ⇒
          val resultStream = reader.readInput(new ByteArrayInputStream(testPrefixedInput.getBytes()),
            charsetTable)

          resultStream should have size 2
          resultStream should contain theSameElementsAs Seq("225563", "225564")
        }
      }
    }

    "correctly handle empty streams" in new Fixture {
      withCharset { charsetTable ⇒
        withTransformationReader(InputDataReader.regexInputTransformation(InputDataReader.ignorePrefixesRegexp)) { reader ⇒
          val resultStream = reader.readInput(new ByteArrayInputStream("".getBytes()),
            charsetTable)

          resultStream should have size 0
        }
      }
    }

    "skip - 5 from start, 5 from end" in new Fixture {
      withCharset { charsetTable ⇒
        withTransformationReader(InputDataReader.regexInputTransformation(InputDataReader.ignorePrefixesRegexp)) { reader ⇒
          val resultStream = reader.readInput(new ByteArrayInputStream(test20Lines0to20Input.getBytes()),
            charsetTable, Some(InputDataReader.Options(startLine = Some(5), endLine = Some(15))))

          resultStream should have size 10
          resultStream should contain theSameElementsAs test20Lines5to15Input.split("\n").map(_.replaceAll("[1|0]", "")).distinct
        }
      }
    }

    "skip 10 lines from the start" in new Fixture {
      withCharset { charsetTable ⇒
        withTransformationReader(InputDataReader.regexInputTransformation(InputDataReader.ignorePrefixesRegexp)) { reader ⇒
          val resultStream = reader.readInput(new ByteArrayInputStream(test20Lines0to20Input.getBytes()),
            charsetTable, Some(InputDataReader.Options(startLine = Some(10))))

          resultStream should have size 10
          resultStream should contain theSameElementsAs test10Lines0to10Input.split("\n").map(_.replaceAll("[1|0]", "")).distinct
        }
      }
    }

    "skip 10 lines from the end" in new Fixture {
      withCharset { charsetTable ⇒
        withTransformationReader(InputDataReader.regexInputTransformation(InputDataReader.ignorePrefixesRegexp)) { reader ⇒
          val resultStream = reader.readInput(new ByteArrayInputStream(test20Lines0to20Input.getBytes()),
            charsetTable, Some(InputDataReader.Options(endLine = Some(10))))

          resultStream should have size 10
          resultStream should contain theSameElementsAs test20Lines10to20Input.split("\n").map(_.replaceAll("[1|0]", "")).distinct
        }
      }
    }
  }

  trait Fixture {
    import utils._

    val test10Lines0to10Input = (22250 to 22261) map { v ⇒ s"$v" } mkString "\n"
    val test20Lines0to20Input = (22250 to 22270) map { v ⇒ s"$v" } mkString "\n"
    val test20Lines10to20Input = (22260 to 22270) map { v ⇒ s"$v" } mkString "\n"
    val test20Lines5to15Input = (22255 to 22265) map { v ⇒ s"$v" } mkString "\n"

    val testInputWithSpaces =
      """
        |      12312312
        |   1233333
        |9999999
      """.stripMargin

    val testPrefixedInput =
      """
        |1-800-225564
        |1-800-225563
      """.stripMargin

    def withCharset(block: CharsetTable ⇒ Unit): Unit = {
      block(CharsetTable(loadInputStream(getClass.getResourceAsStream("/charset.txt"))))
    }

    def withReader(block: InputDataReader ⇒ Unit): Unit = {
      block(new DefaultInputDataReader(None))
    }

    def withTransformationReader(transformation: InputDataReader.InputTransformation)
                                (block: InputDataReader ⇒ Unit): Unit = {
      block(new DefaultInputDataReader(Some(transformation)))
    }

  }

}
