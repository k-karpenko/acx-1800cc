package au.com.aconex.assignment.cc1800

import java.io.{BufferedReader, InputStream, InputStreamReader}

import scala.annotation.tailrec

package object utils {

  def parseOpts(args: Array[String]): Map[String, String] = {
    val (result, _) =
      args.foldLeft[(Map[String, String], Option[String])]((Map[String, String](), None)) {
        case ((map, lastKey), value) ⇒
          if ( value.startsWith("-") ) {
            (map.updated(value, ""), Some(value))
          } else {
            (lastKey.map( v ⇒ map.updated(v, value)).getOrElse(map), lastKey)
          }
      }

    result
  }

  def loadInputStream(is: InputStream): Stream[String] = {
    val reader = new BufferedReader(new InputStreamReader(is))
    val data = loadInputStream0(Option(reader.readLine()), Stream.empty, reader)
    data
  }

  @tailrec
  private def loadInputStream0(tmp: Option[String], buffer: Stream[String], reader: BufferedReader): Stream[String] =
    tmp match {
      case None ⇒ buffer
      case Some("@finish") ⇒ buffer // for a case of System.in using from the IDE console
      case Some(value) ⇒
        loadInputStream0(Option(reader.readLine()),  value #:: buffer, reader)
    }
}
