package au.com.aconex.assignment.cc1800.loaders

import java.io.InputStream

object InputStreamLoader {

  sealed trait Path
  object Path {
    case class ClasspathBased(value: String) extends Path
    case class FilesystemBased(value: String) extends Path
  }

}

trait InputStreamLoader {
  import InputStreamLoader._

  def isAcceptable(path: Path): Boolean

  def openStream(path: Path): Either[Throwable, InputStream]

}
