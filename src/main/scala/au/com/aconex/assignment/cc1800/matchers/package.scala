package au.com.aconex.assignment.cc1800

package object matchers {

  object CharsetTable {

    def apply(lines: Seq[String]): CharsetTable = {
      CharsetTable(
        lines
          .map(_.toLowerCase)
          .map(l ⇒ l.split("="))
          .flatMap(line ⇒
            line(1).trim().split("\\s").map(c ⇒ (c.charAt(0), line(0).trim().charAt(0)))
          )
          .toMap
      )
    }

  }

  case class CharsetTable(data: Map[Char, Char]) {

    def toNumber(value: Seq[Char]): Seq[Char] = value.flatMap(c ⇒ byLetter(c) )

    def byLetter(letter: Char): Option[Char] = data.find(p ⇒ p._1 == letter).map(_._2)

    def byNumber(number: Char): Option[Char] = data.find(p ⇒ p._2 == number).map(_._1)

  }

  object WordsTable {
    def apply(lines: Seq[String], charsetTable: CharsetTable): WordsTable = {
      WordsTable(
        lines
          .map(_.trim())
          .filterNot(_.isEmpty)
          .map( word ⇒
            (word, charsetTable.toNumber(word.toLowerCase))
          )
      )
    }
  }

  case class WordsTable(table: Seq[(String, Seq[Char])])

  type MatchingStrategyProvider = (CharsetTable, WordsTable) ⇒ MatchingStrategy

}
