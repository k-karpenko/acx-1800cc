package au.com.aconex.assignment.cc1800.replacement

object ReplacementStrategy {
  case class Replacement(offset: Int, patch: Seq[Char], patchSize: Int)
}

trait ReplacementStrategy {

  def applyTransformations(line: String, transformations: Iterable[ReplacementStrategy.Replacement]): String

}
