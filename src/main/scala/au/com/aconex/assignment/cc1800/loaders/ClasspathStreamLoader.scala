package au.com.aconex.assignment.cc1800.loaders
import java.io.InputStream

import au.com.aconex.assignment.cc1800.loaders.InputStreamLoader.Path

import scala.util.control.NonFatal

class ClasspathStreamLoader extends InputStreamLoader {
  override def isAcceptable(path: Path) = path match {
    case x: Path.ClasspathBased ⇒ true
    case _ ⇒ false
  }

  override def openStream(path: Path) = path match {
    case x: Path.ClasspathBased ⇒
      try {
        val stream = Option(getClass.getResourceAsStream(x.value))
          .getOrElse(getClass.getClassLoader.getResourceAsStream(x.value))
        if ( stream != null ) Right(stream)
        else Left(new IllegalStateException(s"failed to resolve resource: ${x.value}"))
      } catch {
        case e if NonFatal(e) ⇒ Left(e)
      }
    case path: Any ⇒ Left(new IllegalStateException(s"unsupported path provided: $path"))
  }
}
