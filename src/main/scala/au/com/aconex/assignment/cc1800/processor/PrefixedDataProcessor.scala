package au.com.aconex.assignment.cc1800.processor

import au.com.aconex.assignment.cc1800.loaders.InputStreamLoader.Path
import au.com.aconex.assignment.cc1800.processor.DataProcessor.{DataSource, ProcessResult}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Simple delegation decorator over generic `DataProcessor` which only task is to
  * take over the stream produced by the `delegate` and prefix all its elements with a given
  * `prefix` string.
  */
class PrefixedDataProcessor(prefix: String,
                            delegate: DataProcessor) extends DataProcessor {
  override def process(inputDataPath: DataSource, options: Option[DataProcessor.ProcessOptions])
                      (implicit ec: ExecutionContext): Future[Stream[ProcessResult]] = {
    delegate.process(inputDataPath, options) map { result ⇒
      result map { r ⇒
        r.copy( transformed = prefix + r.transformed)
      }
    }
  }
}
