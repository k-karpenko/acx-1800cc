package au.com.aconex.assignment.cc1800.matchers

import scala.collection.mutable

class DefaultStrategy(wordsDictionary: WordsTable,
                      lettersDictionary: CharsetTable)
  extends MatchingStrategy {
  import MatchingStrategy._

  override def findMatches(line: String): Iterable[MatchingStrategy#Match] = findMatches(line.toCharArray)

  /**
    * Recursive implementation of actual number -> string replacement logic; keep tracking
    * of unchanged symbols amount which uses at the end to decide what type of result should
    * be returned.
    *
    * @param line Source input data
    * @return
    */
  override def findMatches(line: Seq[Char]): Iterable[MatchingStrategy#Match] = {
    val records = mutable.HashSet[List[Result]]()

    def rec(offset: Int, matchRow: List[Result]): List[Result] = {
      offset match {
        case x if x >= line.size ⇒ matchRow
        case _ ⇒
          val words = wordsDictionary.table filter {
            case (_, numerical: Seq[Char]) if numerical.length > 3 ⇒
              line.startsWith(numerical, offset)
            case _ ⇒ false
          }

          if ( words.isEmpty ) rec(offset + 1, Result.NoMatches :: matchRow)
          else {
            words.foldLeft(matchRow) { case (s, word) ⇒
              val subSet = rec(offset + word._2.length,
                Result.Match(offset, word._2.length, word._1) :: matchRow)
              records += subSet
              subSet
            }
          }
      }
    }

    rec(0, List.empty)

    records
  }
}