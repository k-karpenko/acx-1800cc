package au.com.aconex.assignment.cc1800.reader
import java.io.InputStream

import au.com.aconex.assignment.cc1800.matchers.CharsetTable
import au.com.aconex.assignment.cc1800.utils._

class DefaultInputDataReader(transformation: Option[InputDataReader.InputTransformation]) extends InputDataReader {

  private def applyOptions(stream: Stream[String], options: Option[InputDataReader.Options]): Stream[String] = {
    options.map { opts ⇒
      stream.slice(
        opts.startLine.getOrElse(0),
        opts.endLine.getOrElse(stream.length) + 1
      )
    }
    .getOrElse(stream)
  }

  override def readInput(is: InputStream,
                         charsetTable: CharsetTable,
                         options: Option[InputDataReader.Options] = None): Stream[String] = {
    applyOptions(loadInputStream(is), options)
      .map(_.trim)
      .filter(_.nonEmpty)
      .map(r ⇒ transformation.map(_(r)).getOrElse(r))
      .map(c ⇒ c.filter(v ⇒ charsetTable.byNumber(v).nonEmpty) )
      .filter(_.nonEmpty)
      .distinct
  }
}
