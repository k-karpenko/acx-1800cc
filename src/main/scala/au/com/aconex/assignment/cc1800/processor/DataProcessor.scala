package au.com.aconex.assignment.cc1800.processor

import java.io.InputStream

import au.com.aconex.assignment.cc1800.loaders.InputStreamLoader
import au.com.aconex.assignment.cc1800.reader.InputDataReader

import scala.concurrent.{ExecutionContext, Future}

object DataProcessor {
  case class ProcessResult(line: String, transformed: String)

  case class ProcessOptions(maxDistance: Int, readerOptions: Option[InputDataReader.Options] = None)

  case class CharsetTableSource(source: DataSource)
  case class WordsDictionarySource(source: DataSource)

  trait DataSource
  object DataSource {
    case class InputStreamSource(is: InputStream) extends DataSource
    case class PathSource(path: InputStreamLoader.Path) extends DataSource
  }
}

/**
  * Processor is responsible for a work coordination and can be treated as an 'entry-point'
  * to the processing logic.
  *
  * It is up to specific implementation of [[DataProcessor]] how to process input data, unless
  * general contract is followed:
  * - Options passed as an `options` *have* to be applied to the input data/results
  * - All items from the data source given as `inputDataPath` *have* to be included in the processing, unless
  *   other constraints defined in `options`
  * - Async execution if any *have* to be executed by `ec`
  *
  * @author Cyril A. Karpenko <self@nikelin.ru>
  */
trait DataProcessor {

  def process(inputDataPath: DataProcessor.DataSource, options: Option[DataProcessor.ProcessOptions] = None)
             (implicit ec: ExecutionContext): Future[Stream[DataProcessor.ProcessResult]]

}
