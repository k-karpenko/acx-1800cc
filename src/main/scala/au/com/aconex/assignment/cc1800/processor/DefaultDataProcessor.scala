package au.com.aconex.assignment.cc1800.processor

import java.io.InputStream

import au.com.aconex.assignment.cc1800.loaders.InputStreamLoader
import au.com.aconex.assignment.cc1800.reader.InputDataReader
import au.com.aconex.assignment.cc1800.replacement.ReplacementStrategy
import au.com.aconex.assignment.cc1800.matchers.{CharsetTable, MatchingStrategy, MatchingStrategyProvider, WordsTable}
import au.com.aconex.assignment.cc1800.utils

import scala.concurrent.{ExecutionContext, Future}

object DefaultDataProcessor {
  final val defaultMaxDistance = 2
}

class DefaultDataProcessor(charsetTablePath : DataProcessor.CharsetTableSource,
                           wordsDictionaryPath: DataProcessor.WordsDictionarySource,
                           inputStreamLoader: InputStreamLoader,
                           inputStreamReader: InputDataReader,
                           replacementStrategy: ReplacementStrategy,
                           matchingStrategyProvider: MatchingStrategyProvider)
  extends DataProcessor {
  import utils._

  override def process(inputDataPath: DataProcessor.DataSource,
                       options: Option[DataProcessor.ProcessOptions])
                      (implicit ec: ExecutionContext): Future[Stream[DataProcessor.ProcessResult]] =
    Future {
      (
        loadStream(charsetTablePath.source),
        loadStream(wordsDictionaryPath.source),
        loadStream(inputDataPath)
      ) match {
        case (Right(charsetTableStream), Right(wordsDictionaryStream), Right(inputDataStream)) ⇒
          val charsetTable = CharsetTable(loadInputStream(charsetTableStream))
          val wordsTable = WordsTable(loadInputStream(wordsDictionaryStream), charsetTable)

          val strategy = matchingStrategyProvider(charsetTable, wordsTable)


          inputStreamReader.readInput(inputDataStream, charsetTable, options.flatMap(_.readerOptions))
            .flatMap { line ⇒
              val maxDistanceBetweenTerms = options
                .map(_.maxDistance)
                .getOrElse(DefaultDataProcessor.defaultMaxDistance)

              strategy.findMatches(line)
                .foldLeft(Stream.empty[DataProcessor.ProcessResult]) { (l, r) ⇒
                  // i've decided to track just the fact that the max distance between terms
                  // has been reached
                  val unchangedDistance = r.foldLeft(0) {
                    case (counter, MatchingStrategy.Result.NoMatches) ⇒ counter + 1
                    case (counter, _) if counter > maxDistanceBetweenTerms ⇒ counter
                    case (counter, _) ⇒ 0
                  }

                  if (unchangedDistance > maxDistanceBetweenTerms) l
                  else
                      DataProcessor.ProcessResult(line,
                        replacementStrategy.applyTransformations(line, r flatMap {
                          case MatchingStrategy.Result.Match(offset, patchSize, patch) ⇒
                            Some(ReplacementStrategy.Replacement(offset, patch, patchSize))
                          case _ ⇒ None
                        })
                      ) #:: l
                }
            }
        case (Left(e), _, _) ⇒ throw new IllegalStateException("Unable to load charset table", e)
        case (_, Left(e), _) ⇒ throw new IllegalStateException("Unable to load words dictionary", e)
        case (_, _, Left(e)) ⇒ throw new IllegalStateException("Unable to load input data", e)
      }
    }

  protected def loadStream(source: DataProcessor.DataSource): Either[Throwable, InputStream] = {
    source match {
      case DataProcessor.DataSource.InputStreamSource(is) if is == null ⇒
        Left(new IllegalArgumentException("non-null data source provided"))
      case DataProcessor.DataSource.InputStreamSource(is) ⇒ Right(is)
      case DataProcessor.DataSource.PathSource(path) ⇒
        inputStreamLoader.openStream(path)
    }
  }
}
