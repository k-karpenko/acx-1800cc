package au.com.aconex.assignment.cc1800.loaders

import scala.concurrent.ExecutionContext

class CompositeStreamLoader(loaders: Seq[InputStreamLoader])
  extends InputStreamLoader {
  import InputStreamLoader._

  require(loaders.nonEmpty)

  override def isAcceptable(path: Path) = loaders.exists(_.isAcceptable(path))

  override def openStream(path: Path) =
    loaders
      .find(_.isAcceptable(path))
      .map(_.openStream(path))
      .getOrElse(
        Left(new IllegalStateException(s"no stream loader available: $path"))
      )
}
