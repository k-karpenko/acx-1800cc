package au.com.aconex.assignment.cc1800.report

import au.com.aconex.assignment.cc1800.processor.DataProcessor

import scala.concurrent.Future

trait ReportPrinter {

  def produceReport(results: Stream[DataProcessor.ProcessResult]): Future[Unit]

}
