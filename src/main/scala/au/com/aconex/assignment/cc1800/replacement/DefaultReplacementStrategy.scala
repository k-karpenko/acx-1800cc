package au.com.aconex.assignment.cc1800.replacement

class DefaultReplacementStrategy extends ReplacementStrategy {

  override def applyTransformations(line: String, transformations: Iterable[ReplacementStrategy.Replacement]): String = {
    val transformed = transformations.foldLeft((0, line)) {
      case ((increment, l), t@ReplacementStrategy.Replacement(offset, patch, patchSize)) ⇒
        val updatedOffset = offset + increment
        val beforePart = l.substring(0, updatedOffset)
        val afterPart = l.substring(updatedOffset + patchSize)
        val middlePartReplacement = new String(patch.toArray)
        val newLine = beforePart + middlePartReplacement + afterPart
        val diff = increment + middlePartReplacement.length - patchSize
        (diff + 1, newLine)
    }

    transformed._2
  }
}
