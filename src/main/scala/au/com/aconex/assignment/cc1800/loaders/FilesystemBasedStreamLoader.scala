package au.com.aconex.assignment.cc1800.loaders
import java.io.{FileInputStream, InputStream}

import au.com.aconex.assignment.cc1800.loaders.InputStreamLoader.Path

import scala.util.control.NonFatal

class FilesystemBasedStreamLoader extends InputStreamLoader {
  override def isAcceptable(path: Path) = path match {
    case _: Path.FilesystemBased ⇒ true
    case _ ⇒ false
  }

  override def openStream(path: Path) = path match {
    case path: Path.FilesystemBased ⇒
      try {
        Right(new FileInputStream(path.value))
      } catch {
        case e if NonFatal(e) ⇒ Left(e)
      }
    case _ ⇒ Left(new IllegalArgumentException("not supported"))
  }
}
