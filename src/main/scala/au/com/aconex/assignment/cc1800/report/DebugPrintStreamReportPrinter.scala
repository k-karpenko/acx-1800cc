package au.com.aconex.assignment.cc1800.report
import java.io.PrintStream

import au.com.aconex.assignment.cc1800.processor.DataProcessor

import scala.concurrent.{ExecutionContext, Future}

object DebugPrintStreamReportPrinter {
  final val Name = "debug"
}

class DebugPrintStreamReportPrinter(out: PrintStream)(implicit ec: ExecutionContext) extends ReportPrinter {
  override def produceReport(results: Stream[DataProcessor.ProcessResult]): Future[Unit] =
    Future {
      results
        .map {
          case DataProcessor.ProcessResult(original, transformed) ⇒
            (original, transformed)
        }
        .groupBy(_._1)
        .foreach { records ⇒
          out.println(s"Choices for ${records._1}")
          records._2 foreach { choice ⇒
            out.println(s"\t- ${choice._2.toUpperCase()}")
          }
        }
    }
}
