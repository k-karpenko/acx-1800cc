package au.com.aconex.assignment.cc1800

import au.com.aconex.assignment.cc1800.loaders._
import au.com.aconex.assignment.cc1800.processor.{DataProcessor, DefaultDataProcessor, PrefixedDataProcessor}
import au.com.aconex.assignment.cc1800.reader.{DefaultInputDataReader, InputDataReader}
import au.com.aconex.assignment.cc1800.replacement.DefaultReplacementStrategy
import au.com.aconex.assignment.cc1800.report.{DebugPrintStreamReportPrinter, PrintStreamReportPrinter}
import au.com.aconex.assignment.cc1800.matchers.{CharsetTable, DefaultStrategy, WordsTable}

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

object Start {
  import utils._

  private val defaultDictionaryPath = InputStreamLoader.Path.ClasspathBased("charset.txt")
  private val defaultWordsDictionaryPath = InputStreamLoader.Path.ClasspathBased("100k-words.txt")
  private val defaultReportPrinter = PrintStreamReportPrinter.Name

  implicit val ec = ExecutionContext.global

  private val defaultStreamLoaders = Seq(
    new ClasspathStreamLoader(),
    new FilesystemBasedStreamLoader()
  )

  def main(args: Array[String]): Unit = {
    val options = parseOpts(args)

    val isHelp =
      options.get("--help").nonEmpty

    if ( isHelp ) {
      println("1-800- Coding Challenge")
      println("-----------------------------------------------------------------------")
      println("./bin/cc1800 -file /opt/input-data.txt")
      println("./bin/cc1800 < 225533")
      println("-----------------------------------------------------------------------")
      println("-reportPrinter [default | debug]       - change default reports printer")
      println("-verbose [true | false]                - enable verbose output")
      println("-charset [/path/to/data]               - change built-in charset table")
      println("-words [/path/to/data]                 - change default words dictionary")
      println("-file [/path/to/data]                  - path to file with input data to process")
      System.exit(0)
    }

    val isVerbose =
      options.get("--verbose").nonEmpty

    val reportPrinterType = options.get("-reportPrinter")
        .getOrElse(defaultReportPrinter)

    if ( isVerbose ) {
      println(s"reportsPrinter = $reportPrinterType")
    }

    val charsetTablePath =
      options.get("-charset")
        .map(InputStreamLoader.Path.FilesystemBased)
        .getOrElse(defaultDictionaryPath)

    if ( isVerbose ) {
      println(s"charsetTable = $charsetTablePath")
    }

    val wordsTablePath =
      options.get("-words")
        .map(InputStreamLoader.Path.FilesystemBased)
        .getOrElse(defaultWordsDictionaryPath)

    if ( isVerbose ) {
      println(s"wordsTablePath = $wordsTablePath")
    }

    val inputDataPath =
      options.get("-file")
        .map(file ⇒ DataProcessor.DataSource.PathSource(InputStreamLoader.Path.FilesystemBased(file)))
        .getOrElse(DataProcessor.DataSource.InputStreamSource(System.in))

    if ( isVerbose ) {
      println(s"inputDataPath = $inputDataPath")
    }

    val processor =
      new PrefixedDataProcessor(
        "1-800-",
        new DefaultDataProcessor(
          DataProcessor.CharsetTableSource(DataProcessor.DataSource.PathSource(charsetTablePath)),
          DataProcessor.WordsDictionarySource(DataProcessor.DataSource.PathSource(wordsTablePath)),
          new CompositeStreamLoader(defaultStreamLoaders),
          new DefaultInputDataReader(Some(InputDataReader.regexInputTransformation(InputDataReader.ignorePrefixesRegexp))),
          new DefaultReplacementStrategy,
          (charsetTable: CharsetTable, wordsTable: WordsTable) ⇒ new DefaultStrategy(wordsTable, charsetTable)
        )
      )

    val reportPrinter = reportPrinterType match {
      case DebugPrintStreamReportPrinter.Name ⇒ new DebugPrintStreamReportPrinter(System.out)
      case PrintStreamReportPrinter.Name ⇒ new PrintStreamReportPrinter(System.out)
    }

    Await.ready(
      processor.process(inputDataPath) flatMap { results ⇒
        reportPrinter.produceReport(results)
      } recover {
        case e: Throwable ⇒
          e.printStackTrace()
          println("Processing failed")
      },
      100.seconds
    )
  }

}
