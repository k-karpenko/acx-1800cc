package au.com.aconex.assignment.cc1800.reader

import java.io.InputStream

import au.com.aconex.assignment.cc1800.matchers.CharsetTable

object InputDataReader {
  type InputTransformation = String ⇒ String

  val ignorePrefixesRegexp = "((?:1|0)\\-?800\\-?)"

  def inputTransformation(prefixes: String*): InputTransformation = {
    (input: String) ⇒ {
      prefixes.foldLeft(input) { (input, replacement) ⇒
        if (input.startsWith(replacement)) input.substring(replacement.length-1)
        else input
      }
    }
  }

  def regexInputTransformation(expression: String): InputTransformation = {
    (input: String) ⇒ {
      input.replaceAll(expression, "")
    }
  }

  case class Options(startLine: Option[Int] = None, endLine: Option[Int] = None)
}

trait InputDataReader {

  def readInput(is: InputStream, charsetTable: CharsetTable,
                options: Option[InputDataReader.Options] = None): Stream[String]

}
