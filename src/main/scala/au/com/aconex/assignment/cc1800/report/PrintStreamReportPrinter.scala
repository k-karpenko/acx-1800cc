package au.com.aconex.assignment.cc1800.report

import java.io.PrintStream

import au.com.aconex.assignment.cc1800.processor.DataProcessor

import scala.concurrent.{ExecutionContext, Future}

object PrintStreamReportPrinter {
  final val Name = "default"
}

class PrintStreamReportPrinter(out: PrintStream)(implicit ec: ExecutionContext) extends ReportPrinter {
  protected def transformLine(line: String): String = line.toUpperCase

  override def produceReport(results: Stream[DataProcessor.ProcessResult]): Future[Unit] =
    Future {
      results.foreach { record ⇒
        out.println(s"${transformLine(record.transformed)}")
      }
    }
}
