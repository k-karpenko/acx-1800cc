package au.com.aconex.assignment.cc1800.matchers

import au.com.aconex.assignment.cc1800.matchers.MatchingStrategy.Result

object MatchingStrategy {
  sealed trait Result
  object Result {
    case class Match(offset: Int, patchSize: Int, transformed: Seq[Char])
      extends Result

    case object NoMatches extends Result
  }
}

trait MatchingStrategy {
  type Match = List[Result]

  def findMatches(line: String): Iterable[Match]

  def findMatches(line: Seq[Char]): Iterable[Match]
}
